import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: MyApp(),
    theme: themeData,
  ));
}

final ThemeData themeData = ThemeData(
  canvasColor: Colors.blueGrey.shade900,
  accentColor: Colors.blueGrey.shade900, 
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        backgroundColor: Theme.of(ctx).accentColor,
        elevation: 2.0,
      ),
      body: Center(
        child: FlatButton(
          onPressed: (){
            Navigator.push(ctx, Settings());
          },
          child: Text('Settings', style: TextStyle(color: Colors.orange[50]),),
        ),
      ),
    );
  }
}
class Settings extends MaterialPageRoute<Null> {
  Settings() : super(builder: (BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
        backgroundColor: Theme.of(ctx).accentColor,
        elevation: 2.0,
      ),
      body: profileView()
      
    );
  });

}
 Widget profileView() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 0,50),
          child: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: Container(
                width: 350,
                height: 130,
                color: Colors.black54,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                  child: Column(
                    children: <Widget>[
                      CircleAvatar(child: ClipOval(child: Image.asset('assets/images/numan.jpg', height: 100, width: 150, fit: BoxFit.cover,),),),
                      
                      Column(
                       children: <Widget>[
                          Text('Abdullah Al Numan', style: TextStyle(color: Colors.orange[50]),),
                          Text('Software Developer', style: TextStyle(color: Colors.white70),),
                       ]
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Icon(Icons.edit, color: Colors.white70),
                        ),
                    ]
                  )
                ) 
              ),
               ),
            ],
          ),
        ),
        Container(
          child: Column(
           children: <Widget>[
             ListTile(
               title: Text('Grinz Web', style: TextStyle(color: Colors.orange[50]),),
               subtitle: Text('QR code', style: TextStyle(color: Colors.white70),),
               leading: Icon(Icons.language, color: Colors.white70),
             ),
             ListTile(
               title: Text('Grup List', style: TextStyle(color: Colors.orange[50]),),
               leading: Icon(Icons.group, color: Colors.white70),
             ),
             ListTile(
               title: Text('Appreance', style: TextStyle(color: Colors.orange[50]),),
               subtitle: Text('Theme, Task Background, Chat Background', style: TextStyle(color: Colors.white70),),
               leading: Icon(Icons.animation, color: Colors.white70),
             ),
             ListTile(
               title: Text('Linked Devices', style: TextStyle(color: Colors.orange[50]),),
               leading: Icon(Icons.devices, color: Colors.white70),
             ),
             ListTile(
               title: Text('Language', style: TextStyle(color: Colors.orange[50]),),
               leading: Icon(Icons.language, color: Colors.white70),
             ),
             ListTile(
               title: Text('Account', style: TextStyle(color: Colors.orange[50]),),
               subtitle: Text('Privacy, security, change number', style: TextStyle(color: Colors.white70),),
               leading: Icon(Icons.attribution, color: Colors.white70),
             ),
             ListTile(
               title: Text('Notification', style: TextStyle(color: Colors.orange[50]),),
               subtitle: Text('Task, Message', style: TextStyle(color: Colors.white70),),
               leading: Icon(Icons.notifications, color: Colors.white70),
             ),
             
           ]
          ),
        ),  
      ],
    );
  }